# maya_abc_material_export

#### 介绍
maya abc材质导入导出

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.info文化解析
导出的info文件是一个json文件。
keys: 
    poly_matDic：记录几何体与材质名称的映射关系
        {"geo1":"shadername1","geo2":"shadername2"}
    
    ai_attrDic：记录几何体上maya中的Arnold面板上的一些属性值
        {"geo1":{"aiMatte":0,"aiQpaque":0,...}}
    
    前面两个对于maya原生材质导入导出就行了
    mat_pathDic: 这个是记录材质路径信息的。 原来只记录标准surface的材质路径，与displacement置换图，现在需要根据材质类型不同，进行材质属性迁移
        {"shadername":{"shader_type":"aiCarPaint","attr":{"base_color":[0.94,0.94,0.95],"melanin_redness":0.5,..},"path":{"opacity":"R://FNDH/Asset_work/xx/a.jpg","base_color":"R:..."}},
         "shadername2":{},...}

2.houdini中生成Arnold材质


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
